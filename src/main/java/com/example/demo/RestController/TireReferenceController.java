package com.example.demo.RestController;


import com.example.demo.Domain.TireReference;
import com.example.demo.Service.TireReferenceService;
import org.springdoc.core.annotations.ParameterObject;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("api/tires")
public class TireReferenceController {

    @Autowired
    private TireReferenceService tireReferenceService;

    // build create TireReference REST API
    @PostMapping
    public ResponseEntity<TireReference> createTire(@RequestBody TireReference tireReference) {
        TireReference savedTireReference = tireReferenceService.createTire(tireReference);
        return new ResponseEntity<>(savedTireReference, HttpStatus.CREATED);
    }

    // build get tire by id REST API
    // http://localhost:8080/api/tires/1
    @GetMapping("{id}")
    public ResponseEntity<TireReference> getTireById(@PathVariable("id") UUID tireId) {
        TireReference tireReference = tireReferenceService.getTireById(tireId);
        return new ResponseEntity<>(tireReference, HttpStatus.OK);
    }

    // Build Get All Tires REST API
    // http://localhost:8080/api/tires
    @GetMapping
    public ResponseEntity<List<TireReference>> getAllTires() {
        List<TireReference> tireReferences = tireReferenceService.getAllTires();
        return new ResponseEntity<>(tireReferences, HttpStatus.OK);
    }


    // http://localhost:8080/api/tires/search
    @GetMapping("/search")
    @PageableAsQueryParam
    public ResponseEntity<Page<TireReference>> searchTires(@RequestParam(name="name",required = false) String name,
                                                           @PageableDefault(page = 0, size = 20)
                                                  @SortDefault(sort = "name", direction = Sort.Direction.ASC)
                                                  @ParameterObject Pageable pageable) {
        Page<TireReference> tires = tireReferenceService.searchTires(name,pageable);
        return new ResponseEntity<>(tires, HttpStatus.OK);
    }

    // Build Update TireReference REST API
    @PutMapping("{id}")
    // http://localhost:8080/api/tires/1
    public ResponseEntity<TireReference> updateTire(@PathVariable("id") UUID tireId,
                                                    @RequestBody TireReference tireReference) {
        tireReference.setId(tireId);
        TireReference updatedTireReference = tireReferenceService.updateTire(tireReference);
        return new ResponseEntity<>(updatedTireReference, HttpStatus.OK);
    }

    // Build Delete TireReference REST API
    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteTire(@PathVariable("id") UUID tireId) {
        tireReferenceService.deleteTire(tireId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}