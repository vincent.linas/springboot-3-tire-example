package com.example.demo.Repository;

import com.example.demo.Domain.TireReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TireRepository extends JpaRepository<TireReference, UUID> {

    Page<TireReference> findAllByNameContains(String name, Pageable pageable);
    Page<TireReference> findAll(Pageable pageable);

}

