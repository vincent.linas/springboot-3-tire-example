package com.example.demo.Service;

import com.example.demo.Domain.TireReference;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.UUID;

public interface TireReferenceService {
        TireReference createTire(TireReference tireReference);

        TireReference getTireById(UUID id);

        List<TireReference> getAllTires();


        Page<TireReference> searchTires(String name, Pageable pageable);

        TireReference updateTire(TireReference tireReference);

        void deleteTire(UUID id);
}

