package com.example.demo.Service;

import com.example.demo.Domain.Batch;
import com.example.demo.Domain.TireReference;
import com.example.demo.Repository.BatchRepository;
import com.example.demo.Repository.TireRepository;
import io.micrometer.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TireReferenceServiceImpl implements TireReferenceService {

    @Autowired
    private TireRepository tireRepository;

    @Autowired
    private BatchRepository batchRepository;

    @Override
    public TireReference createTire(TireReference tireReference) {
        for(Batch batch: tireReference.getBatches()){
            //remove batch's id if it's not one already present
            //made to avoid collisions
            batch.setId(null);
            batchRepository.save(batch);
        }
        return tireRepository.save(tireReference);
    }

    @Override
    public TireReference getTireById(UUID id) {
        Optional<TireReference> optionalTire = tireRepository.findById(id);
        return optionalTire.get();
    }

    @Override
    public List<TireReference> getAllTires() {
        return tireRepository.findAll();
    }

    public Page<TireReference> searchTires(String name, Pageable pageable) {
        if (StringUtils.isBlank(name)){
            return tireRepository.findAll(pageable);
        }
        return tireRepository.findAllByNameContains(name,pageable);
    }

    @Override
    public TireReference updateTire(TireReference tireReference) {
        TireReference existingTireReference = tireRepository.findById(tireReference.getId()).get();
        existingTireReference.setPrice(tireReference.getPrice());
        existingTireReference.setRemainingQuantity(tireReference.getRemainingQuantity());
        existingTireReference.setName(tireReference.getName());
        existingTireReference.setSize(tireReference.getSize());

        for(Batch batch: tireReference.getBatches()){
            //remove batch's id if it's not one already present
            //made to avoid collisions
            if(!existingTireReference.getBatches().contains(batch)){
                batch.setId(null);
            }
            batchRepository.save(batch);
        }
        existingTireReference.setBatches(tireReference.getBatches());
        TireReference updatedTireReference = tireRepository.save(existingTireReference);
        return updatedTireReference;
    }

    @Override
    public void deleteTire(UUID id) {
        tireRepository.deleteById(id);
    }
    
}
